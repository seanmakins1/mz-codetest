**MarshallZehr code challenge**

By Sean Makins

Bank of Canada APi is implemented but it made me curious about other implementations of this data resource requirement. I kept it in as the results were quite different, and showed couple ways to tackle a task.

**Back end** is a .Net Core Api

**Front end** I thought I would do something a little different, I built it out using the front end framework "Blazor" it works exactly like Angular/Vue/React but instead of JS it is built entirely in C#. And no, it doesn't just compile down to Javascript, it actually runs in Web Assembly. Concepts are the same, just thought it would be something cool to checkout, and show case some really cool new stacks coming out.

**To run the application** from Visual Studio just make sure the MZ-CodeTest.Server is the start up project and you can just run it with IIS Express, or you can just run MZ-CodeTest.Server (will spin up a console app) and navigate to  https://localhost:5001


