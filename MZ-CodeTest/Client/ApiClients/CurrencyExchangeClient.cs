﻿using Microsoft.AspNetCore.Components;
using MZ_CodeTest.Shared.CurrencyExchange;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace MZ_CodeTest.Client.ApiClients
{
    public class CurrencyExchangeClient
    {
        private HttpClient _http { get; }

        private readonly Dictionary<string, string> _symbolsByCode;

        public CurrencyExchangeClient(HttpClient http)
        {
            _http = http;
            _symbolsByCode = new Dictionary<string, string>();

            var regions = CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                          .Select(x => new RegionInfo(x.LCID));

            foreach (var region in regions)
                if (!_symbolsByCode.ContainsKey(region.ISOCurrencySymbol))
                    _symbolsByCode.Add(region.ISOCurrencySymbol, region.CurrencySymbol);
        }

        public string FormatCurrency(string iso4217, decimal value)
        {
            return GetSymbol(iso4217)
                + " "
                + Math.Round(value, 4)
                .ToString();
        }

        private string GetSymbol(string code) { return _symbolsByCode[code]; }

        public async Task<ExchangeRates> LoadExchangeRates(string date = "")
        {
            return await _http.GetFromJsonAsync<ExchangeRates>($"api/currency-exchange/from-cad/{date}");
        }


        public async Task<BoCExchangeData> GetExchangeRates(string from, string to, DateTime? date)
        {
            var boc = BoCExchangeData.create(from, to, date);
            return await _http.PostJsonAsync<BoCExchangeData>("api/currency-exchange/exchange", boc);
        }




    }
}
