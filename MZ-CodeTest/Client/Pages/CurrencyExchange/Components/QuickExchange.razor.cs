﻿using Microsoft.AspNetCore.Components;
using MZ_CodeTest.Client.ApiClients;
using MZ_CodeTest.Shared.CurrencyExchange;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MZ_CodeTest.Client.Pages.CurrencyExchange.Components
{
    public partial class QuickExchange
    {
        [Inject] CurrencyExchangeClient _client { get; set; }

        ExchangeRates exchangeRates { get; set; }

        Dictionary<string, decimal> currentValues { get; set; }


        DateTime selectedDate { get; set; } = DateTime.Today;


        decimal amount { get; set; }

        string message { get; set; } = string.Empty;

        protected async override Task OnInitializedAsync()
        {
            exchangeRates = await _client.LoadExchangeRates();
            currentValues = exchangeRates.rates.GetExchanges(0);
        }


        private async Task changeDate(ChangeEventArgs args)
        {
            if (DateTime.TryParse((string)args.Value, out DateTime date))
            {
                selectedDate = date;
                exchangeRates = await _client.LoadExchangeRates($"{date:yyyy-MM-dd}");
                currentValues = exchangeRates.rates.GetExchanges(amount);
            }
        }


        private void amountChanged(ChangeEventArgs args)
        {
            updateExchange((string)args.Value);
        }

        void updateExchange(string value)
        {
            if (decimal.TryParse(value,
                System.Globalization.NumberStyles.Currency,
                System.Globalization.NumberFormatInfo.CurrentInfo,
                out decimal amt))
            {
                amount = amt;
                currentValues = exchangeRates.rates.GetExchanges(amt);
                message = string.Empty;
            }
            else
            {
                message = !string.IsNullOrWhiteSpace(value)
                    ? message = $"{ value } is not a valid number."
                    : "";

            }
        }
    }
}
