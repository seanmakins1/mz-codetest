﻿using Microsoft.AspNetCore.Components;
using MZ_CodeTest.Client.ApiClients;
using MZ_CodeTest.Shared.CurrencyExchange;
using MZ_CodeTest.Shared.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MZ_CodeTest.Client.Pages.CurrencyExchange.Components
{
    public partial class BocExchange
    {

        [Inject] CurrencyExchangeClient _client { get; set; }
        DateTime selectedDate { get; set; } = DateTime.Today;


        decimal amount { get; set; }

        string message { get; set; } = string.Empty;

        string to { get; set; } = CurrencyCodes.codes.First();

        string from { get; set; } = CurrencyCodes.codes.First();

        BoCExchangeData currentExchange { get; set; }

        string convertedAmount { get; set; }

        bool isLoading { get; set; } = false;

        protected async override Task OnInitializedAsync()
        {
            await loadRates();
            await base.OnInitializedAsync();
        }

        private async Task changeDate(ChangeEventArgs args)
        {
            if (DateTime.TryParse((string)args.Value, out DateTime date))
            {
                selectedDate = date;
                await loadRates();
            }
        }

        private async Task changeFrom(ChangeEventArgs e)
        {
            from = e.Value.ToString();
            await loadRates();
        }

        private async Task changeTo(ChangeEventArgs e)
        {
            to = e.Value.ToString();
            await loadRates();
        }

        private async Task loadRates()
        {
            isLoading = true;
            await Task.Yield();
            if (from != to)
            {
                currentExchange = await _client.GetExchangeRates(from, to, selectedDate);
                updateExchange();
            }
            else
            {
                currentExchange = BoCExchangeData.create(from, to, selectedDate);
                updateExchange();
            }
            isLoading = false;
        }

        private void amountChanged(ChangeEventArgs args)
        {
            if (decimal.TryParse((string)args.Value,
               System.Globalization.NumberStyles.Currency,
               System.Globalization.NumberFormatInfo.CurrentInfo,
               out decimal amt))
            {
                amount = amt;
                message = string.Empty;
                updateExchange();
            }
            else
            {
                message = !string.IsNullOrWhiteSpace((string)args.Value)
                    ? message = $"{ (string)args.Value } is not a valid number."
                    : "";

            }
        }

        void updateExchange()
        {
            var amt = _client
                .FormatCurrency(currentExchange.To, 
                    amount * currentExchange.Rate);

            var original = _client
                .FormatCurrency(currentExchange.From,
                    amount);


            convertedAmount = $"{original} {currentExchange.From} converts to {amt} {currentExchange.To}";
        }
    }
}
