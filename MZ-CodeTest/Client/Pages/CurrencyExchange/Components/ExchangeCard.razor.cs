﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MZ_CodeTest.Client.Pages.CurrencyExchange.Components
{
    public partial class ExchangeCard
    {
        [Parameter]
        public string currency { get; set; }

        [Parameter]
        public string amount { get; set; }
    }
}
