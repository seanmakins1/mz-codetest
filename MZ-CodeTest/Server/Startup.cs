using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MZ_CodeTest.Server.Services;
using MZ_CodeTest.Server.Services.CurrencyExchange.BankOfCanada;
using System;
using System.Linq;

namespace MZ_CodeTest.Server
{
    public class Startup
    {
        public IConfiguration config { get; }

        public Startup(IConfiguration configuration) => config = configuration;

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient("CurrencyApi", c =>
            {
                c.BaseAddress = new Uri(config.GetValue<string>("CurrencyApiBaseUrl"));
            });
            services.AddHttpClient("BankOfCanadaApi", c =>
            {
                c.BaseAddress = new Uri(config.GetValue<string>("BankOfCanadaApiBaseUrl"));
            });
            services.AddSingleton<ICurrencyExchangeService, CurrencyExchangeService>();
            services.AddSingleton<IBankOfCanadaExchangeService, BankOfCanadaExchangeService>();
            services.AddControllersWithViews();
            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
            });
        }
    }
}
