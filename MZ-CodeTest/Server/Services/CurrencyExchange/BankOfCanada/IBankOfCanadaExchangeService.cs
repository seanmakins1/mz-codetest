﻿using MZ_CodeTest.Shared.CurrencyExchange;
using System.Threading.Tasks;

namespace MZ_CodeTest.Server.Services.CurrencyExchange.BankOfCanada
{
    public interface IBankOfCanadaExchangeService
    {
        Task<BoCExchangeData> GetExchange(BoCExchangeData exchange);
    }
}