﻿using MZ_CodeTest.Shared.CurrencyExchange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace MZ_CodeTest.Server.Services.CurrencyExchange.BankOfCanada
{
    public class BankOfCanadaExchangeService : IBankOfCanadaExchangeService
    {
        private HttpClient _bocApi { get; }

        public BankOfCanadaExchangeService(IHttpClientFactory httpClientFactory)
        {
            _bocApi = httpClientFactory.CreateClient("BankOfCanadaApi");
        }

        public async Task<BoCExchangeData> GetExchange(BoCExchangeData exchange)
        {
            if (exchange.ExchangeDate == DateTime.Today)
            {
                var response = await _bocApi.GetAsync($"observations/FX{exchange.From}{exchange.To}?recent=1");

                using var jsonResponse = JsonDocument.Parse(await response.Content.ReadAsStringAsync());

                exchange.Label = jsonResponse.RootElement.GetProperty("seriesDetail")
                    .GetProperty($"FX{exchange.From}{exchange.To}")
                    .GetProperty("description").GetString();

                var payload = jsonResponse.RootElement.GetProperty("observations")[0];

                exchange.AsOf = "As of " + payload.GetProperty("d").GetString();

                if (decimal.TryParse(payload
                    .GetProperty($"FX{exchange.From}{exchange.To}")
                    .GetProperty("v").GetString(), out decimal rate)) exchange.Rate = rate;


                return exchange;
            }
            else
            {
                // grabbing historic dates up to selected date incase it is not available (weekends, holidays?) 
                var sdt = exchange.ExchangeDate.AddDays(-7).ToString("yyyy-MM-dd");
                var edt = exchange.ExchangeDate.ToString("yyyy-MM-dd");

                var response = await _bocApi.GetAsync($"observations/FX{exchange.From}{exchange.To}?start_date={sdt}&end_date={edt}");

                using var jsonResponse = JsonDocument.Parse(await response.Content.ReadAsStringAsync());

                var info = jsonResponse.RootElement.GetProperty("seriesDetail")
                    .GetProperty($"FX{exchange.From}{exchange.To}")
                    .GetProperty("description").GetString();

                var rate = jsonResponse.RootElement.GetProperty("observations")
                    .EnumerateArray()
                    .Last();

                exchange.AsOf = "As of " + rate.GetProperty("d").GetString();
                if (decimal.TryParse(rate
                    .GetProperty($"FX{exchange.From}{exchange.To}")
                    .GetProperty("v").GetString(), out decimal foundRate))
                    exchange.Rate = foundRate;

                return exchange;
            }

        }

    }
}
