﻿using MZ_CodeTest.Shared.CurrencyExchange;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace MZ_CodeTest.Server.Services
{
    public class CurrencyExchangeService : ICurrencyExchangeService
    {
        private HttpClient _currencyApi { get; }
        public CurrencyExchangeService(IHttpClientFactory httpClientFactory)
        {
            _currencyApi = httpClientFactory.CreateClient("CurrencyApi");
        }

        public async Task<ExchangeRates> ConvertFromCanadian(string date)
        {
            return (string.IsNullOrWhiteSpace(date))
                ? await _currencyApi.GetFromJsonAsync<ExchangeRates>("latest?base=CAD")
                : await _currencyApi.GetFromJsonAsync<ExchangeRates>($"{date}?base=CAD");
        }

        public async Task<ExchangeRates> ConvertToCanadian(string baseCurrency, string date)
        {
            return (string.IsNullOrWhiteSpace(date))
                ? await _currencyApi.GetFromJsonAsync<ExchangeRates>($"latest?base={baseCurrency}")
                : await _currencyApi.GetFromJsonAsync<ExchangeRates>($"{date}?base={baseCurrency}");
        }
    }
}
