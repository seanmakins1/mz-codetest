﻿using MZ_CodeTest.Shared.CurrencyExchange;
using System.Threading.Tasks;

namespace MZ_CodeTest.Server.Services
{
    public interface ICurrencyExchangeService
    {
        Task<ExchangeRates> ConvertFromCanadian(string date);
        Task<ExchangeRates> ConvertToCanadian(string baseCurrency, string date);
    }
}