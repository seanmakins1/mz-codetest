﻿using Microsoft.AspNetCore.Mvc;
using MZ_CodeTest.Server.Services;
using MZ_CodeTest.Server.Services.CurrencyExchange.BankOfCanada;
using MZ_CodeTest.Shared.CurrencyExchange;
using System;
using System.Threading.Tasks;

namespace MZ_CodeTest.Server.Controllers
{
    [ApiController]
    [Route("api/currency-exchange")]
    public class CurrencyExchangeController : ControllerBase
    {
        private ICurrencyExchangeService _currencyExchangeService { get; }
        public IBankOfCanadaExchangeService _bankOfCanadaExchangeService { get; }

        public CurrencyExchangeController(
            ICurrencyExchangeService currencyExchangeService, 
            IBankOfCanadaExchangeService bankOfCanadaExchangeService)
        {
            _currencyExchangeService = currencyExchangeService;
            _bankOfCanadaExchangeService = bankOfCanadaExchangeService;
        }


        [HttpGet]
        [Route("from-cad/{date?}")]
        public async Task<ExchangeRates> ConvertFromCAD (string date = null)
        {
            try
            {
                return await _currencyExchangeService.ConvertFromCanadian(date);
            }
            catch
            {
                // Exception handling should never be this general.. 
                // log to some implementation of ILogger etc..

                return null;
            }
        }

        [HttpPost]
        [Route("exchange")]
        public async Task<BoCExchangeData> ConvertToCAD([FromBody] BoCExchangeData boc)
        {
            try
            {
                return await _bankOfCanadaExchangeService.GetExchange(boc);
            }
            catch
            {
                // Exception handling should never be this general.. 
                // log to some implementation of ILogger etc..
                boc.Label = $"Bank of Canada is unable to convert from {boc.From} to {boc.To} :(";
                return boc;
            }
        }
    }


}
