﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace MZ_CodeTest.Shared.Helpers
{
    public class CurrencyCodes
    {
        public static readonly IList<string> codes = new ReadOnlyCollection<string>(new List<string>
        {
            "CAD",
            "USD",
            "AUD",
            "BRL",
            "CNY",
            "EUR",
            "HKD",
            "INR",
            "IDR",
            "JPY",
            "MXN",
            "NZD",
            "NOK",
            "PEN",
            "MYR",
            "RUB",
            "SAR",
            "SGD",
            "ZAR",
            "KRW",
            "SEK",
            "CHF",
            "TWD",
            "THB",
            "TRY",
            "GBP",
            "VND",
        });
    }
}
