﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MZ_CodeTest.Shared.CurrencyExchange
{
    public class BoCExchangeData
    {
        public string From { get; set; }

        public string To { get; set; }

        public DateTime ExchangeDate { get; set; }

        public decimal Rate { get; set; } = 1;

        public string Label { get; set; }

        public string AsOf { get; set; }

        public static BoCExchangeData create(string from, string to, DateTime? date)
        {
            return new BoCExchangeData()
            {
                From = from,
                To = to,
                ExchangeDate = date ?? DateTime.Today
            };
        }
    }
}
